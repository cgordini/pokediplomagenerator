﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PokeDiplomaGenerator.Backend;

namespace PokeDiplomaGenerator.GUI
{
	public partial class MainWindow : Form
	{
		private Constants.Versions version;

		public MainWindow()
		{
			InitializeComponent();
		}

		private void MainWindow_Load(object sender, EventArgs e)
		{
			comboBox1.SelectedIndex = 0;
			//version = Constants.Versions.PokemonRed;
			//Set textBox1 so it's blank somehow?
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			if (!String.IsNullOrEmpty(textBox1.Text))
				Generator.Generate(textBox1.Text, version);

			//Save file dialog box?
		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			//Add in support for 
			switch (comboBox1.SelectedIndex)
			{
				case 0:
					version = Constants.Versions.PokemonRB;
					break;
				case 1:
					version = Constants.Versions.PokemonYellow;
					break;
				case 2:
					version = Constants.Versions.PokemonGSC;
					break;
				default:
					break;
			}
		}

		private void textBox1_Changed(object sender, System.EventArgs e)
		{
			//Run some additional conditionals for invalid chars?
			if (textBox1.TextLength < 9 && textBox1.TextLength > 0)
			{
				// Clear the error, if any, in the error provider.
				errorProvider1.SetError(this.textBox1, String.Empty);
				label3.Text = String.Empty;
				btnSave.Enabled = true;
			}
			else if (textBox1.TextLength <= 0)
			{
				// Set the error if the name is not valid.
				errorProvider1.SetError(this.textBox1, " ");
				label3.Text = "Please enter in a name.";
				btnSave.Enabled = false;
			}
			else
			{
				// Set the error if the name is not valid.
				errorProvider1.SetError(this.textBox1, " ");
				label3.Text = "Please use less than 9 characters.";
				btnSave.Enabled = false;
			}
		}

	}
}

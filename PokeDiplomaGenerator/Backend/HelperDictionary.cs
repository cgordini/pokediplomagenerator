﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PokeDiplomaGenerator.Properties;

namespace PokeDiplomaGenerator.Backend
{
	class HelperDictionary
	{
		public readonly Dictionary<char, System.Drawing.Bitmap> _dictionary = new Dictionary<char, System.Drawing.Bitmap>();
		public readonly Dictionary<Constants.Versions, System.Drawing.Image> _versionDict 
																= new Dictionary<Constants.Versions, System.Drawing.Image>();

		//doesn't need to have a ctor if static?
		public HelperDictionary()
		{
			#region Letters and Chars

			//add upper case
			_dictionary.Add('A', Resources.A);
			_dictionary.Add('B', Resources.B);
			_dictionary.Add('C', Resources.C);
			_dictionary.Add('D', Resources.D);
			_dictionary.Add('E', Resources.E);
			_dictionary.Add('F', Resources.F);
			_dictionary.Add('G', Resources.G);
			_dictionary.Add('H', Resources.H);
			_dictionary.Add('I', Resources.I);
			_dictionary.Add('J', Resources.J);
			_dictionary.Add('K', Resources.K);
			_dictionary.Add('L', Resources.L);
			_dictionary.Add('M', Resources.M);
			_dictionary.Add('N', Resources.N);
			_dictionary.Add('O', Resources.O);
			_dictionary.Add('P', Resources.P);
			_dictionary.Add('Q', Resources.Q);
			_dictionary.Add('R', Resources.R);
			_dictionary.Add('S', Resources.S);
			_dictionary.Add('T', Resources.T);
			_dictionary.Add('U', Resources.U);
			_dictionary.Add('V', Resources.V);
			_dictionary.Add('W', Resources.W);
			_dictionary.Add('X', Resources.X);
			_dictionary.Add('Y', Resources.Y);
			_dictionary.Add('Z', Resources.Z);

			//lower case
			_dictionary.Add('a', Resources.a1);
			_dictionary.Add('b', Resources.b1);
			_dictionary.Add('c', Resources.c1);
			_dictionary.Add('d', Resources.d1);
			_dictionary.Add('e', Resources.e1);
			_dictionary.Add('f', Resources.f1);
			_dictionary.Add('g', Resources.g1);
			_dictionary.Add('h', Resources.h1);
			_dictionary.Add('i', Resources.i1);
			_dictionary.Add('j', Resources.j1);
			_dictionary.Add('k', Resources.k1);
			_dictionary.Add('l', Resources.l1);
			_dictionary.Add('m', Resources.m1);
			_dictionary.Add('n', Resources.n1);
			_dictionary.Add('o', Resources.o1);
			_dictionary.Add('p', Resources.p1);
			_dictionary.Add('q', Resources.q1);
			_dictionary.Add('r', Resources.r1);
			_dictionary.Add('s', Resources.s1);
			_dictionary.Add('t', Resources.t1);
			_dictionary.Add('u', Resources.u1);
			_dictionary.Add('v', Resources.v1);
			_dictionary.Add('w', Resources.w1);
			_dictionary.Add('x', Resources.x1);
			_dictionary.Add('y', Resources.y1);
			_dictionary.Add('z', Resources.z1);

			//CHARS
			_dictionary.Add(' ', Resources.space);

			#endregion

			#region Versions

			_versionDict.Add(Constants.Versions.PokemonRB, Resources.DiplomaRB);
			_versionDict.Add(Constants.Versions.PokemonYellow, Resources.DiplomaY);
			_versionDict.Add(Constants.Versions.PokemonGSC, Resources.DiplomaGSC);

			#endregion
		}
	}
}

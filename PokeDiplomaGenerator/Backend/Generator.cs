﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using PokeDiplomaGenerator.Properties;
using PokeDiplomaGenerator.GUI;

namespace PokeDiplomaGenerator.Backend
{
	class Generator
	{
		/// <summary>
		/// ENTRY POINT
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainWindow());
		}//end main

		public static void Generate(string player, Constants.Versions version)
		{
			//make initial diploma, locations
			//conditionalize based on version value?
			int x = 0;
			int y = 0;
			int i = 0;

			if (version == Constants.Versions.PokemonGSC)
			{
				x = 288;
				y = 160;
			}
			else //RBY
			{
				x = 320;
				y = 128;
			}

			//establish array of letters
			Bitmap[] letters = new Bitmap[player.Length];
			HelperDictionary dict = new HelperDictionary();

			foreach (char c in player.ToCharArray())
			{
				dict._dictionary.TryGetValue(c, out letters[i]);
				i++;
			}

			//fetch the diploma from the version dictionary
			Image tempDiploma;
			dict._versionDict.TryGetValue(version, out tempDiploma);

			using (Image diploma = new Bitmap(tempDiploma))
			{
				Graphics editable = Graphics.FromImage(diploma);

				foreach (var letter in letters)
				{
					using (Graphics graphic = Graphics.FromImage(letter))
					{
						Bitmap temp = new Bitmap(letter);

						editable.DrawImage(temp, new Point(x, y));

						//shift capital whitespace
						x = x + 32;
					}
				}
				//In the future, give user option to save in other locations
				diploma.Save("diploma.jpeg", ImageFormat.Jpeg);
			}
		} //end generator
	}
}